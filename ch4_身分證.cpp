#include <stdio.h>
#include <stdlib.h> //亂數相關函數
#include <time.h>   //時間相關函數
/*身分證字號共10碼：1大寫英文字母+9數字
(1)英文轉換數字： 
	A=10 台北市　　　J=18 新竹縣　　　S=26 高雄縣
	B=11 台中市　　　K=19 苗栗縣　　　T=27 屏東縣
	C=12 基隆市　　　L=20 台中縣　　　U=28 花蓮縣
	D=13 台南市　　　M=21 南投縣　　　V=29 台東縣
	E=14 高雄市　　　N=22 彰化縣　　  W=32 金門縣
	F=15 新北市　    O=35 新竹市　　　X=30 澎湖縣
	G=16 宜蘭縣　　　P=23 雲林縣　　　Y=31 陽明山
	H=17 桃園縣　　　Q=24 嘉義縣　　  Z=33 連江縣
	I=34 嘉義市　    R=25 台南縣　
(2)首位數字為生理性別：男1、女2
(3)規則：
	1.將英文轉成的數字,拆為十位數、個位數兩碼 
	2.把每一個數字依序乘上1、9、8、7、6、5、4、3、2、1、1，最後再相加 mod10 為檢查碼=0為合法字號 
*/
int main(){
	int eng;   //身份證字號英文部分的數字 
	char trans; //數字轉英文 
    int num[9]; //身份證字號第2~10碼數字部分 
  	srand(time(NULL)); //設定亂數種子，使每次亂數出來的結果不相同 
  	
  	//英文：10~35的整數亂數 
  	int min=10, max=35;
  	eng = rand() % (max - min + 1) + min; 
  	int sum = eng/10 + 9*(eng%10);
  	
  	//數字第1碼：產生1or2的整數亂數
  	min=1, max=2;
  	num[0] = rand() % (max - min + 1) + min; 
  	sum += 8*num[0];
  	
  	//第2~8碼數字：產生0~9的整數亂數
	min=0, max=9;	
	
	for (int i=1;i<9;i++){
		num[i] = rand() % (max - min + 1) + min; 
		sum += (8-i)*num[i];
	}
	
	//最後1碼由前9碼決定合法數字 (檢查碼=0) 
	num[8]=10-sum%10;
	
	//輸出時將第一碼數字轉為英文 
	if(eng>=10 && eng <= 17)
		trans = eng+55;
	else if(eng>=18 && eng <= 22)
		trans = eng+56;
	else if(eng>=23 && eng <= 29)
		trans = eng+57;
	else if(eng>=30 && eng <= 31)
		trans = eng+58;	
	else if(eng==34)
		trans='I';
	else if(eng==35)
		trans='O';	
	else if(eng==32)
		trans='W';
	else if(eng==33)
		trans='Z';
	
	//printf("%d",eng);
	printf("%c",trans);
	for (int i=0;i<9;i++)
		printf("%d",num[i]);
		
    return 0;
}
