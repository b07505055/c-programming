#include <stdio.h>
#define LIMIT 30

int main()
{
    int n,arr[LIMIT][LIMIT],diag[2]={0};
    
    printf("Please input the size of array: ");
    scanf("%d",&n);
	printf("Input: ");
	
    for(int i=0;i<n;++i)
        for(int j=0;j<n;++j)
            scanf("%d", &arr[i][j]);
	for(int i=0;i<n;++i) {
        for(int j=0;j<n;++j) {
        	if(j==n-1)
           		printf("%2d", arr[i][j]);
           	else printf("%2d ", arr[i][j]);
        }
        printf("\n");
    }
	
    printf("\nRow sums:");
    for(int i=0;i<n;++i)
    {
        int temp = 0;
        for(int j=0;j<n;++j)
            temp += arr[i][j];
        printf(" %d",temp);
    }

    printf("\nColumn sums:");
    for(int i=0;i<n;++i)
    {
        int temp = 0;
        for(int j=0;j<n;++j)
            temp += arr[j][i];
		printf(" %d",temp);
    }
	

    for(int i=0;i<n;++i)
    {
		diag[0] += arr[i][i];
		diag[1] += arr[i][n-i-1];
    }
	printf("\nDiagonal sums: %d %d\n",diag[0],diag[1]); 
    
    return 0;
}
