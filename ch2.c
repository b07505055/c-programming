#include <stdio.h>
#include <stdlib.h>

int main()
{
	float amount[4],rate,pay;
    scanf("%f %f %f", &amount[0],&rate,&pay);
    for(int i=0;i<4;i++){
    	amount[i+1] = amount[i]-pay+amount[i]/12*0.01*rate;
	}
    printf("Balance remaining after first payment: $%.2f\n",amount[1]);
    printf("Balance remaining after second payment: $%.2f\n",amount[2]);
    printf("Balance remaining after third payment: $%.2f\n",amount[3]);

    return 0;
}
