#include <stdio.h>

int main()
{
    int d1, m1, y1, d2, m2, y2, day1, day2;
	
    scanf("%d/%d/%d", &m1, &d1, &y1);
    // while(0<m1 && m1<=12 && )
	day1 = d1 + m1*30 + y1*365;

    if (day1==0){
        printf("\n");
        return 0;
    }

    while (day1 != 0) {
        scanf("%d/%d/%d", &m2, &d2, &y2);
        day2 = d2 + m2*30 + y2*365;
        if (day2==0){
        	printf("%d/%d/%02d is the earliest date\n",m1, d1, y1);
        	return 0;
    	}
        if(day2<day1){
        	d1=d2;
        	m1=m2;
        	y1=y2;
        	day1=day2;
		}
    }

    return 0;
}
